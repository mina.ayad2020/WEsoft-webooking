package com.wesoft.webooking;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.wesoft.webooking.Adapters.LogInPagerAdapter;

public class LogInActivity extends BaseActivity {

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new LogInPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);





    }
}
