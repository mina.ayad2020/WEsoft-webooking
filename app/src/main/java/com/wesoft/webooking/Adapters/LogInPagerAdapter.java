package com.wesoft.webooking.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.wesoft.webooking.Faragments.SignInPageFragment;
import com.wesoft.webooking.Faragments.SignUpPageFragment;

public class LogInPagerAdapter extends FragmentStatePagerAdapter {
    public LogInPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new SignInPageFragment();

            case 1:
                return new SignUpPageFragment();

        }
        return new SignInPageFragment();
    }

    @Override
    public int getCount() {
        return 2;
    }
}